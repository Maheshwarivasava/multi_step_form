const toggleButton = document.querySelector(".toggle_button");
const toggleButtonIcon = document.querySelector(".toggle_button i");
const dropdownMenu = document.querySelector(".dropdown_menu");

toggleButton.onclick = function () {
  dropdownMenu.classList.toggle("open");
  const isOpen = dropdownMenu.classList.contains("open");

  toggleButtonIcon.classList = isOpen
    ? "fa-solid fa-bars"
    : "fa-solid fa-bars-staggered";
};

document.querySelector('.login-btn').addEventListener('click', function() {
  // console.log("Hii");
  document.getElementById('login-modal').style.display = 'flex';
  
  });
  
  document.querySelector('.login-btn-mobile').addEventListener('click', function() {
    // console.log("Hii");
      document.getElementById('login-modal').style.display = 'flex';
      document.querySelector('.dropdown_menu').style.display = 'none';
    });

  document.querySelector('.close').addEventListener('click', function() {
    document.getElementById('login-modal').style.display = 'none';
  });
  
  window.addEventListener('click', function(event) {
    if (event.target == document.getElementById('login-modal')) {
      document.getElementById('login-modal').style.display = 'none';
    }
  });
  
//checking user input values 
function checkName() {
  let myNameInput = document.getElementById("infoName");
  let myNameError = document.getElementById("errorName");
  let myNameRegex = /^[a-zA-Z-\s]+$/;

  if (myNameInput.value.trim() == "") {
    myNameError.innerHTML = "This field is required";
  } else if (myNameRegex.test(myNameInput.value) == false) {
    myNameError.innerHTML = "Can't contain numbers or symbols";
  } else if (myNameInput.value.trim() !== "") {
    myNameError.innerHTML = "";
  }
}

function checkNum() {
  let myNumInput = document.getElementById("infoNumber");
  let myNumError = document.getElementById("errorNum");
  let myNumRegex = /^[0-9\s]+$/;

  if (myNumInput.value.trim() == "") {
    myNumError.innerHTML = "This field is required";
  } else if (myNumRegex.test(myNumInput.value) == false) {
    myNumError.innerHTML = "Must contain number";
  } else if (myNumInput.value.trim().length !== 10) {
    myNumError.innerHTML = "Invalid Phone Number";
  } else {
    myNumError.innerHTML = "";
  }
}

function checkMail() {
  let myMailInput = document.getElementById("infoMail");
  let myNumError = document.getElementById("errorMail");
  let myMailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (myMailInput.value.trim() == "") {
    myNumError.innerHTML = "This field is required";
  } else if (myMailRegex.test(myMailInput.value) == false) {
    myNumError.innerHTML = "Please enter a valid e-mail";
  } else {
    myNumError.innerHTML = "";
  }
}
function Achieve() {
  let myNumInput = document.getElementById("infoNumber");
  let myNameInput = document.getElementById("infoName");
  let myMailInput = document.getElementById("infoMail");
  let myNameRegex = /^[a-zA-Z-\s]+$/;
  let myNumRegex = /^[0-9\s]+$/;
  let myMailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (
    myNumInput.value.trim().length === 10 &&
    myNumRegex.test(myNumInput.value) == true &&
    myMailInput.value.trim().length !== 0 &&
    myMailRegex.test(myMailInput.value) === true &&
    myNameInput.value.trim().length !== 0 &&
    myNameRegex.test(myNameInput.value) === true
  ) {
    stepInfo.style.display = "none";
    stepPlan.style.display = "flex";
    circle1.style.color = "white";
    circle1.style.backgroundColor = "rgba(255, 255, 255, 0)";
    circle2.style.backgroundColor = "rgb(255, 255, 255)";
    circle2.style.color = "black";
  }
}
//function for going to part two of the form
function goStepTwo() {
  checkName();
  checkNum();
  checkMail();
  Achieve();
}
//function for going to part one of the form
function backStepOne() {
  stepInfo.style.display = "flex";
  stepPlan.style.display = "none";
  circle1.style.color = "black";
  circle1.style.backgroundColor = "rgba(255, 255, 255)";
  circle2.style.backgroundColor = "rgb(255, 255, 255, 0)";
  circle2.style.color = "white";
}
const planPrice = "0";
console.log(planPrice);

function getPrice1() {
  const planPrice = "9";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}

function getPrice2() {
  const planPrice = "12";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}
function getPrice3() {
  const planPrice = "15";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}
function getPrice4() {
  const planPrice = "90";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}
function getPrice5() {
  const planPrice = "120";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}
function getPrice6() {
  const planPrice = "150";
  console.log(planPrice);
  document.getElementById("totalPrice").innerHTML = planPrice;
}
function reset() {
  //toggle button
  monthArcade.style.backgroundColor = "white";
  monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
  monthPro.style.backgroundColor = "white";
  monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
  monthAdvance.style.backgroundColor = "white";
  monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
  yearlyBasic.style.backgroundColor = "white";
  yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
  yearlyAdvance.style.backgroundColor = "white";
  yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
  yearlyPro.style.backgroundColor = "white";
  yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";

  document.getElementById("onlineServiceMonth").checked = false;
  document.getElementById("storageMois").checked = false;
  document.getElementById("customizableMois").checked = false;
  document.getElementById("onlineAnnee").checked = false;
  document.getElementById("storageAnnee").checked = false;
  document.getElementById("customizableYearly").checked = false;
  document.getElementById("onlinePrice").innerHTML = "+0$";
  document.getElementById("storagePrice").innerHTML = "+0$";
  document.getElementById("customizablePrice").innerHTML = "+0$";
  document.getElementById("onlinePrice").innerHTML = "+0$";
  document.getElementById("storagePrice").innerHTML = "+0$";
  document.getElementById("customizablePrice").innerHTML = "+0$";
  document.getElementById("modeResume").innerHTML = "Choose a plan";
  document.getElementById("priceResume").innerHTML = "0$";
 
}
checkBox = document
  .getElementById("switch")
  .addEventListener("click", event => {
    if (event.target.checked) {
      planYear.style.display = "flex";
      planMonth.style.display = "none";
      addOnYaerly.style.display = "flex";
      addOnMonth.style.display = "none";
    } else {
      planYear.style.display = "none";
      planMonth.style.display = "flex";
      addOnYaerly.style.display = "none";
      addOnMonth.style.display = "flex";
    }
  });
  checkBox = document
  .getElementById("monthArcade")
  .addEventListener("click", event => {
    monthArcade.style.backgroundColor = "hsl(217, 100%, 97%)";
    monthArcade.style.border = " solid 1px hsl(213, 96%, 18%)";
    /*change the background color and border color */
    monthAdvance.style.backgroundColor = "white";
    monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthPro.style.backgroundColor = "white";
    monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyBasic.style.backgroundColor = "white";
    yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyAdvance.style.backgroundColor = "white";
    yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyPro.style.backgroundColor = "white";
    yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    /* change the price  per month */
    document.getElementById("modeResume").innerHTML = "Arcade (monthly)";
    document.getElementById("priceResume").innerHTML = "9$/mo";
    document.getElementById("modeTotal").innerHTML = "Total (per month)";
  });
  checkBox = document
  .getElementById("monthAdvance")
  .addEventListener("click", event => {
    monthAdvance.style.backgroundColor = "hsl(217, 100%, 97%)";
    monthAdvance.style.border = " solid 1px hsl(213, 96%, 18%)";
   /*change the background color and border color */
    monthArcade.style.backgroundColor = "white";
    monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthPro.style.backgroundColor = "white";
    monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyBasic.style.backgroundColor = "white";
    yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyAdvance.style.backgroundColor = "white";
    yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyPro.style.backgroundColor = "white";
    yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    /* change the price  per month */
    document.getElementById("modeResume").innerHTML = "Advanced (monthly)";
    document.getElementById("priceResume").innerHTML = "12$/mo";
    document.getElementById("modeTotal").innerHTML = "Total (per month)";
  });
  checkBox = document
  .getElementById("monthPro")
  .addEventListener("click", event => {
    monthPro.style.backgroundColor = "hsl(217, 100%, 97%)";
    monthPro.style.border = " solid 1px hsl(213, 96%, 18%)";
    /*change the background color and border color */
    monthAdvance.style.backgroundColor = "white";
    monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthArcade.style.backgroundColor = "white";
    monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyBasic.style.backgroundColor = "white";
    yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyAdvance.style.backgroundColor = "white";
    yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyPro.style.backgroundColor = "white";
    yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    /* change the price  per month */
    document.getElementById("modeResume").innerHTML = "Pro (monthly)";
    document.getElementById("priceResume").innerHTML = "15$/mo";
    document.getElementById("modeTotal").innerHTML = "Total (per month)";
  });
  checkBox = document
  .getElementById("yearlyBasic")
  .addEventListener("click", event => {
    yearlyBasic.style.backgroundColor = "hsl(217, 100%, 97%)";
    yearlyBasic.style.border = " solid 1px hsl(213, 96%, 18%)";
    /*change the background color and border color */
    monthArcade.style.backgroundColor = "white";
    monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthPro.style.backgroundColor = "white";
    monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthAdvance.style.backgroundColor = "white";
    monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyAdvance.style.backgroundColor = "white";
    yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyPro.style.backgroundColor = "white";
    yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";
   /* change the price  per year */
    document.getElementById("modeResume").innerHTML = "Arcade (yearly)";
    document.getElementById("priceResume").innerHTML = "90$/yr";
    document.getElementById("modeTotal").innerHTML = "Total (per year)";
  });
  checkBox = document
  .getElementById("yearlyAdvance")
  .addEventListener("click", event => {
    yearlyAdvance.style.backgroundColor = "hsl(217, 100%, 97%)";
    yearlyAdvance.style.border = " solid 1px hsl(213, 96%, 18%)";
    /*change the background color and border color */
    monthAdvance.style.backgroundColor = "white";
    monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthPro.style.backgroundColor = "white";
    monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthArcade.style.backgroundColor = "white";
    monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyBasic.style.backgroundColor = "white";
    yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyPro.style.backgroundColor = "white";
    yearlyPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    /* change the price  per year */
    document.getElementById("modeResume").innerHTML = "Advanced (yearly)";
    document.getElementById("priceResume").innerHTML = "12O$/yr";
    document.getElementById("modeTotal").innerHTML = "Total (per year)";
  });
  checkBox = document
  .getElementById("yearlyPro")
  .addEventListener("click", event => {
    yearlyPro.style.backgroundColor = "hsl(217, 100%, 97%)";
    yearlyPro.style.border = " solid 1px hsl(213, 96%, 18%)";
    /*change the background color and border color */
    monthArcade.style.backgroundColor = "white";
    monthArcade.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthPro.style.backgroundColor = "white";
    monthPro.style.border = " solid 1px hsl(229, 24%, 87%)";
    monthAdvance.style.backgroundColor = "white";
    monthAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyBasic.style.backgroundColor = "white";
    yearlyBasic.style.border = " solid 1px hsl(229, 24%, 87%)";
    yearlyAdvance.style.backgroundColor = "white";
    yearlyAdvance.style.border = " solid 1px hsl(229, 24%, 87%)";
    /* change the price  per year */
    document.getElementById("modeResume").innerHTML = "Pro (yearly)";
    document.getElementById("priceResume").innerHTML = "150$/yr";
    document.getElementById("modeTotal").innerHTML = "Total (per year)";
  });
 

  //step 3
  function goStepThree() {
    


    console.log(typeof document.getElementById("totalPrice").innerHTML);

   if (document.getElementById("totalPrice").innerHTML == "") {
    console.log(document.getElementById("totalPrice").innerHTML);
    document.getElementById("totalPrice").innerHTML = "0";
    console.log(document.getElementById("totalPrice").innerHTML);
   }
    checkPlan();
  }
  function checkPlan() {
    if (document.getElementById("modeResume").innerHTML == "Choose a plan") {
      stepInfo.style.display = "flex";
      stepPlan.style.display = "none";
      circle1.style.color = "white";
      circle1.style.backgroundColor = "rgba(255, 255, 255, 0)";
      circle2.style.backgroundColor = "rgb(255, 255, 255)";
      circle2.style.color = "black";
    } else {
      stepPlan.style.display = "none";
      stepAddOn.style.display = "flex";
      circle2.style.color = "white";
      circle2.style.backgroundColor = "rgba(255, 255, 255, 0)";
      circle3.style.backgroundColor = "rgb(255, 255, 255)";
      circle3.style.color = "black";
    }
  }
  function backSteptTwo() {
    stepPlan.style.display = "flex";
    stepAddOn.style.display = "none";
    circle2.style.color = "black";
    circle2.style.backgroundColor = "rgba(255, 255, 255)";
    circle3.style.backgroundColor = "rgb(255, 255, 255, 0)";
    circle3.style.color = "white";
  }

// function for addOns
checkBox = document
  .getElementById("onlineServiceMonth")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("onlinePrice").innerHTML = "+1$/mo";
      document.getElementById("modeTotal").innerHTML = "Total (per month)";
    } else {
      document.getElementById("onlinePrice").innerHTML = "+0$";
    }
  });
checkBox = document
  .getElementById("storageMois")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("storagePrice").innerHTML = "+2$/mo";
      document.getElementById("modeTotal").innerHTML = "Total (per month)";
    } else {
      document.getElementById("storagePrice").innerHTML = "+0$";
    }
  });
checkBox = document
  .getElementById("customizableMois")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("customizablePrice").innerHTML = "+2$/mo";
      document.getElementById("modeTotal").innerHTML = "Total (per month)";
    } else {
      document.getElementById("customizablePrice").innerHTML = "+0$";
    }
  });
checkBox = document
  .getElementById("onlineAnnee")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("onlinePrice").innerHTML = "+10$/yr";
      document.getElementById("modeTotal").innerHTML = "Total (per year)";
    } else {
      document.getElementById("onlinePrice").innerHTML = "+0$";
    }
  });
checkBox = document
  .getElementById("storageAnnee")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("storagePrice").innerHTML = "+20$/yr";
      document.getElementById("modeTotal").innerHTML = "Total (per year)";
    } else {
      document.getElementById("storagePrice").innerHTML = "+0$";
    }
  });
checkBox = document
  .getElementById("customizableYearly")
  .addEventListener("click", event => {
    if (event.target.checked) {
      document.getElementById("customizablePrice").innerHTML = "+20$/yr";
      document.getElementById("modeTotal").innerHTML = "Total (per year)";
    } else {
      document.getElementById("customizablePrice").innerHTML = "+0$";
    }
  });

  //step-4
  function goStepFour() {
    stepSummary.style.display = "flex";
    stepAddOn.style.display = "none";
    circle3.style.color = "white";
    circle3.style.backgroundColor = "rgba(255, 255, 255, 0)";
    circle4.style.backgroundColor = "rgb(255, 255, 255)";
    circle4.style.color = "black";
  
    const adOnnPrice = document.getElementsByName("adOnn").forEach(radio => {
      if (radio.checked) {
        console.log(radio.value);
        console.log(document.getElementById("totalPrice").innerHTML);
        const planPrice = document.getElementById("totalPrice").innerHTML;
        console.log(planPrice);
        document.getElementById("totalPrice").innerHTML =
          parseInt(planPrice) + parseInt(radio.value);
      }
    });
  }

  function backStepThree() {
    stepSummary.style.display = "none";
    stepAddOn.style.display = "flex";
    circle3.style.color = "black";
    circle3.style.backgroundColor = "rgba(255, 255, 255)";
    circle4.style.backgroundColor = "rgb(255, 255, 255, 0)";
    circle4.style.color = "white";
  
    const adOnnPrice = document.getElementsByName("adOnn").forEach(radio => {
      if (radio.checked) {
        console.log(radio.value);
        console.log(document.getElementById("totalPrice").innerHTML);
        const planPrice = document.getElementById("totalPrice").innerHTML;
        console.log(planPrice);
        document.getElementById("totalPrice").innerHTML =
          parseInt(planPrice) - parseInt(radio.value);
      }
    });
  }

  function goFromFourToTwo() {
    stepSummary.style.display = "none";
    stepPlan.style.display = "flex";
    circle2.style.color = "black";
    circle2.style.backgroundColor = "rgba(255, 255, 255)";
    circle4.style.backgroundColor = "rgb(255, 255, 255, 0)";
    circle4.style.color = "white";
  
    const adOnnPrice = document.getElementsByName("adOnn").forEach(radio => {
      if (radio.checked) {
        console.log(radio.value);
        console.log(document.getElementById("totalPrice").innerHTML);
        const planPrice = document.getElementById("totalPrice").innerHTML;
        console.log(planPrice);
        document.getElementById("totalPrice").innerHTML =
          parseInt(planPrice) - parseInt(radio.value);
      }
    });
  }
  function goToStepThankYou() {
    console.log(document.getElementById("modeResume").innerHTML);
    if (document.getElementById("modeResume").innerHTML == "Choose a plan") {
      stepSummary.style.display = "flex";
      stepThankYou.style.display = "none";
    } else {
      stepSummary.style.display = "none";
      stepThankYou.style.display = "flex";
    }
  }
  
  